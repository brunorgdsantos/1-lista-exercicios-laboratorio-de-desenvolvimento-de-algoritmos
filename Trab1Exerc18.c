/*Quest�o 18:
Ler 10 n�meros a serem digitados pelo usu�rio e armazen�-los em um vetor.
a) Exibir a quantidade de vezes que o n�mero 3 est� presente no vetor.*/

#include<stdio.h>

int main(){
        /* Objetivo: Ler 10 numeros, armazena-los em um vetor e dizer o numero de apari��es de 3;
        1- Declarar quantro vari�veis do tipo int;
        2- Usar a estrutura de repeti��o do for;
        3- Mostrar quantos numeros 3 foram digitados*/

        int i = 0, k = 0, x = 0;
        int num[10];

        printf("Digite dez numeros:\n");

        for(i = 0; i < 10; i++){
            scanf(" %d", &num[i]);
        }
        for(k = 0; k < 10; k++){
            if(num[k] == 3){
             x++;
            }
        }
        printf("Numero de vezes que aparece o 3: %d", x);
        return 0;
}






