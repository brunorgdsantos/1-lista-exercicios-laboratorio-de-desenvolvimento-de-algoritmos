/*Quest�o 10:
Leia o ano de nascimento de um nadador, calcule sua idade, e classifique-o em uma das
categorias:
 * Infantil A --- de 5 a 7 anos
 * Infantil B --- de 8 a 10 anos
 * Juvenil A --- de 11 a 13 anos
 * Juvenil B --- de 14 a 17 anos
 * S�nior --- maiores de 17 anos
Obs.: Utilize como ano atual o ano de 2011.
O programa deve fornecer uma sa�da do tipo:
Nadador de idade �idade� � da categoria �categoria�*/

#include<stdio.h>

int main(){
        /* Objetivo: Classificar um nadador em uma determinada categoria;
        1- Declarar duas vari�veis do tipo int (ano_nascimento, idade);
        2- Leitura das variaveis digitada pelo scanf;
        3- Usar a estrutura condicional switch case;
        4- Mostrar o resultado segundo uma categoria*/

        int ano_nascimento, idade;

        printf("Qual seu ano de nascimento?\n");
        scanf("%d", &ano_nascimento);

        idade = 2011 - ano_nascimento;

        switch(idade){
            case 5:
            case 6:
            case 7: printf("Nadador de idade %d e da categoria Infantil A", idade);
            break;
            case 8:
            case 9:
            case 10: printf("Nadador de idade %d e da categoria Infantil B", idade);
            break;
            case 11:
            case 12:
            case 13: printf("Nadador de idade %d e da categoria Juvenil A", idade);
            break;
            case 14:
            case 15:
            case 16:
            case 17: printf("Nadador de idade %d e da categoria Juvenil B", idade);
            break;
            default: printf("Erro");
            break;
        }
        return 0;
}






