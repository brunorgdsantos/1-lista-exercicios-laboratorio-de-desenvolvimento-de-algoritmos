/*Quest�o 9:
Uma companhia quer verificar se um empregado est� qualificado para a aposentadoria.
Para estar em condi��es, um dos seguintes requisitos deve ser satisfeito:
 * Ter no m�nimo 65 anos de idade.
 * Ter trabalhado, no m�nimo 30 anos.
 * Ter no m�nimo 60 anos e ter trabalhado no m�nimo 25 anos.
Ler os dados: o ano de nascimento do empregado e o ano de seu ingresso na companhia.
O programa dever� escrever a idade e o tempo de trabalho do empregado e a mensagem
�Requerer aposentadoria� ou �N�o requerer�. Obs.: Utilize como ano atual o ano de 2011.*/

#include<stdio.h>

int main(){
        /* Objetivo: Verificar se um empregado est� qualificado para a aposentadoria;
        1- Declarar tr�s vari�veis do tipo int (idade, tempo_trabalho);
        2- Leitura das variaveis digitada pelo scanf;
        3- Usar estrutura condicional if para os requisitos;
        4- Mostrar o resultado ap�s an�lise*/

        int ano_nasci, ano_ingresso, calc_tempo_trabalho, calculo_idade;

        printf("Digite o ano de nascimento e o tempo de trabalho respectivamente:\n");
        scanf("%d%d", &ano_nasci, &ano_ingresso);

        calculo_idade = 2011 - ano_nasci;
        calc_tempo_trabalho = 2011 - ano_ingresso;


        if(calculo_idade >= 65 || calc_tempo_trabalho >= 30){
            printf("Requerer aposentadoria");
            }else if(calculo_idade >= 60 && calc_tempo_trabalho >= 25){
                printf("Requerer aposentadoria");
                }else printf("N�o requerer");

        return 0;
}





