/*Quest�o 11:
Fa�a algoritmo que funcione como uma calculadora entre dois n�meros, e que possua as
4 opera��es b�sicas: soma, subtra��o, divis�o e multiplica��o. O programa deve perguntar
ao usu�rio qual opera��o ele quer realizar, a resposta do usu�rio dever� ser um caractere,
'+' se soma, '-' se subtra��o, '*' se multiplica��o, e '/' se divis�o. Em seguida o programa
deve pedir para o usu�rio digitar o primeiro n�mero e depois o segundo. Como sa�da o
programa deve exibir o resultado da opera��o realizada.
a) fa�a utilizando if/else.
b) fa�a utilizando switch/case.*/

#include<stdio.h>

int main(){
        /* Objetivo: Retornar o resultado entre dois numeros conforme opera��o escolhida pelo usu�rio;
        1- Declarar tr�s vari�veis, uma do tipo char e duas do tipo int (operacao, var1, var2);
        2- Leitura das variaveis digitada pelo scanf;
        3- Usar a estrutura condicional if else;
        4- Mostrar o resultado*/

        char operacao;
        int var1, var2;

        printf("Qual operacao voce quer realizar?\n");
        scanf("%c", &operacao);

        printf("Digite o primeiro numero:\n");
        scanf(" %d", &var1);

        printf("Digite o segundo numero:\n");
        scanf(" %d", &var2);

        switch (operacao){
        case '+': printf("Resultado: %d", (var1+var2));
        break;
        case '-': printf("Resultado: %d", (var1-var2));
        break;
        case '*': printf("Resultado: %d", (var1*var2));
        break;
        case '/': printf("Resultado: %d", (var1/var2));
        break;
        }
        return 0;
}
