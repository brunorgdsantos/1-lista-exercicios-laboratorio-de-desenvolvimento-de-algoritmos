/*Quest�o 2: Que pe�a ao usu�rio para digitar um caractere, e em seguida exiba esse caractere na tela.
a) exiba como caractere (%c).
b) exiba como inteiro (%d).
c) exiba como hexadecimal (%x).*/

#include<stdio.h>

int main(){
        /* Objetivo: Digitar um caractere e em seguida exibir esse caractere na tela
        1- Declarar uma vari�vel do tipo char
        2- Leitura do caractere digitado pelo scanf;
        3- Exibi��o do numero digitado pelo printf usando %c, %d, %x*/

        char carac;

        scanf("%c", &carac);

        printf("%c\n", carac);
        printf("%d\n", carac);
        printf("%x\n", carac);
        return 0;
}
