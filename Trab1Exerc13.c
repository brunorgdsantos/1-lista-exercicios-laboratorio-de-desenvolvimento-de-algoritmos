/*Quest�o 13:
Exiba todos os n�meros entre 32 e 126. Exiba um n�mero por linha, mas em cada linha
mostre o n�mero em tr�s formatos: inteiro (%d), hexadecimal (%x), e caractere (%c).
Utilize a estrutura de repeti��o for na resolu��o do problema. Compare o resultado com a
tabela ascii abaixo.*/

#include<stdio.h>

int main(){
        /* Objetivo: Exibir os numeros no intervalo 32 a 126 nos formatos solicitados;
        1- Declarar duas variaveis do tipo int (var1 = 32, i = 0);
        2- Usar a estrutura de repeti��o for;
        3- Mostrar o resultado*/

        int var1 = 32, i = 0;

        for(var1 = 32; var1 <= 126; i++){
            printf("Formatos: Inteiro %d, Hexadecimal %x, Caractere %c\n", var1, var1, var1);
            var1++;
        }
        return 0;
}

