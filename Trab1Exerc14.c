/*Quest�o 14:
Leia dois valores, e exiba sua soma. Em seguida pergunte ao usu�rio: �Novo C�lculo
(S/N)?�. Deve-se ler a resposta e se a resposta for �S� (sim), deve-se repetir todos os
comandos (instru��es) novamente, mas se for qualquer outra resposta, o algoritmo deve
ser finalizado escrevendo a mensagem �Fim dos C�lculos�.*/

#include<stdio.h>

int main(){
        /* Objetivo: Ler dois valores e exibir sua soma;
        1- Declarar tr�s vari�veis do tipo char e int (carac ,var1, var2);
        2- Usar a estrutura de repeti��o do while;
        3- Mostrar o resultado conforme a instru��o do usu�rio*/

        char carac;
        int var1 = 0, var2 = 0;

        do{
            printf("Digite dois numeros:\n");
            scanf("%d%d", &var1, &var2);

            printf("Soma: %d\n", (var1+var2));

            printf("Novo Calculo(S/N)?\n");
            scanf(" %c", &carac);
        }while(carac == 's' || carac == 'S');

        printf("Fim dos calculos");
        return 0;
}


