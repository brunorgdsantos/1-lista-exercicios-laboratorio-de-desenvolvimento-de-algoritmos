/*Quest�o 15:
Desenhe na tela uma forma geom�trica utilizando caracteres, utilize um espa�o entre os
caracteres. Sugest�o de caracteres: X, 0, O.
b)Pergunte ao usu�rio qual o tamanho do quadrado que ele quer que seja desenhado, e o
desenhe.*/

#include<stdio.h>

int main(){
        /* Objetivo: Desenhar na tela uma forma geometrica do tamanho especificado pelo usu�rio;
        1- Declarar uma vari�vel do tipo char;
        2- Declarar tr�s vari�veis do tipo int (i, x, var1);
        3- Usar a estrutura de repeti��o for;
        4- Mostrar o resultado conforme solicitado pela quest�o*/

        char carac;
        int i = 0, x = 0, var1;

        printf("Escolha um caractere: Sugestao X, 0, O\n");
        scanf("%c", &carac);

        printf("Qual o tamanho do quadrado que voce quer que seja desenhado?\n");
        scanf("%d", &var1);
        printf("\n\n");

        for(i = 0; i < var1; i++){
            for(x = 0; x < var1; x++){
                printf("%c ", carac);
            }
            printf("\n");
        }
        return 0;
}



