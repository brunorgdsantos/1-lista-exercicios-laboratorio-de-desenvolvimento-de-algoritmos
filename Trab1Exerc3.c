/*Quest�o 3: Ler duas notas e exibir a m�dia aritm�tica.*/

#include<stdio.h>

int main(){
        /* Objetivo: Exibir a m�dia aritm�tica;
        1- Declarar tr�s vari�veis do tipo float (nota1, nota2 e media);
        2- Leitura das notas digitado pelo scanf;
        3- Exibi��o da m�dia pelo printf */

        float nota1, nota2, media;

        scanf("%f%f", &nota1, &nota2);
        media = (nota1+nota2)/2 ;
        printf("%.2f\n", media);
        return 0;
}

