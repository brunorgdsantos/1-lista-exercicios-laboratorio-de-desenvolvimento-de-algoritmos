/*Quest�o 17:
Leia dois n�meros inteiros, o primeiro representar� a base e o segundo o expoente. Calcule
o valor da base elevado ao expoente. Exemplo: 34 = 3*3*3*3 = 81; 53 = 5*5*5 = 125. Em
C n�o existe nenhum operador matem�tico que calcule o expoente. Desenvolva o seu
pr�prio algoritmo para realizar o c�lculo, utilizando um la�o de repeti��o para realizar
sucessivas multiplica��es.*/

#include<stdio.h>

int main(){
        /* Objetivo: Calcular o valor da base elevado ao expoente;
        1- Declarar tr�s vari�veis do tipo int;
        2- Usar a estrutura de repeti��o do for;
        3- Mostrar o resultado conforme solicitado pela quest�o*/

        int var1 = 0, var2 = 0, var3 = 0, calc = 0, i = 0, w = 1;

        printf("Digite dois numeros inteiros:\n");
        scanf("%d%d", &var1, &var2);

        for(i = 0; i < var2; i++){
            printf("%d.", var1);
            calc = var1;

            for(w = 1; w < var2; w++){
                var3 = var1;
                calc = calc * var3;
            }
        }
        printf("= %d", calc);
        return 0;
}





