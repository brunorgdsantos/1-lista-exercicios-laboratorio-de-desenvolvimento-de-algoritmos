/*Quest�o 20 :
Criar uma aplica��o para ler um nome. Em seguida exiba o nome de tr�s formas diferentes.
c) caractere a caractere em ordem decrescente dos �ndices (do fim para o come�o).*/

#include<stdio.h>
#include<string.h>

int main(){
        /* Objetivo: Criar uma plica��o que leia um nome e exiba-o conforme solicitado;
        1- Declarar tr�s variaveis do tipo int e char;
        2- Usar gets;
        3- Usar estrutura de repeti��o for*/

        int i = 0;
        int a = 0;
        char nome[100];

        printf("Digite um nome: ");
        gets(nome);
        printf("Nome digitado: %s\n\n", nome);

        a = strlen(nome);
        i = a;

        for(i = a; i >= 0; i--){
            printf("%c\n", nome[i]);
        }
        return 0;
}

