/*Quest�o 20 :
Criar uma aplica��o para ler um nome. Em seguida exiba o nome de tr�s formas diferentes.
a) texto normal, utilizando a m�scara de formata��o string (%s).*/

#include<stdio.h>

int main(){
        /* Objetivo: Criar uma plica��o que leia um nome e exiba-o conforme solicitado;
        1- Declarar tr�s variaveis do tipo int e char;
        2- Usar gets*/

        char num[50];

        printf("Digite um nome: ");
        gets(num);
        printf("Nome digitado: %s", num);
        return 0;
}

