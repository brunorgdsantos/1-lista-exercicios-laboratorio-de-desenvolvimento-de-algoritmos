/*Quest�o 4 MATRIX:
Leia 2 notas de 10 alunos. N�o � para ler o Nome. As notas devem ser armazenadas em
uma matriz 2D onde a primeira coluna � a nota1, e a segunda coluna a nota2. Em seguida,
exiba a tabela.
Adicione uma terceira coluna a sua matriz, e armazene nela a m�dia das 2 notas. Aten��o
a m�dia deve ser calculada automaticamente pelo seu programa, o usu�rio n�o ir� digitar a
m�dia, apenas as 2 notas.*/

#include<stdio.h>

int main(){
        /* Objetivo: Gerar uma matriz de inteiros preenchida pelo usu�rio, nota1 e nota2 e informar a media;
        1- Declarar sete vari�veis do tipo int;
        2- Usar a estrutura de repeti��o do for e condicinais if;
        3- Mostrar a matriz informando qual foi o maior n�mero*/

        int i = 0, k = 0, m = 1, w = 0;
        int nota1[20], nota2[20];
        float media[20];

        for(i = 0; i < 10; i++){
            for(k = 0; k < 1; k++){
                printf("Aluno %d - Nota 1: ", m);
                scanf(" %d", &nota1[w]);
                printf("Aluno %d - Nota 2: ", m);
                scanf(" %d", &nota2[w]);
                printf("\n");
                w++;
                m++;
            }
        }
        i = 0, k = 0, m = 0, w = 0;
        for(i = 0; i < 10; i++){
            for(k = 0; k < 1; k++){
                media[w] = nota1[w]+nota2[w];
                printf("%d - %d - %.2f", nota1[w], nota2[w], media[w]/2.0);
                printf("\n");
                w++;
            }
        }
        return 0;
}

