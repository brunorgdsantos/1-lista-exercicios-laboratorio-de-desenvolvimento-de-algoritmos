/*Quest�o 20 :
Criar uma aplica��o para ler um nome. Em seguida exiba o nome de tr�s formas diferentes.
b) caractere a caractere em ordem crescente dos �ndices (do come�o para o fim).*/

#include<stdio.h>
#include<string.h>

int main(){
        /* Objetivo: Criar uma plica��o que leia um nome e exiba-o conforme solicitado;
        1- Declarar tr�s variaveis do tipo int e char;
        2- Usar gets ;
        3- Usar estrutura de repeti��o for*/

        int i = 0;
        int a = 0;
        char nome[100];

        printf("Digite um nome: ");
        gets(nome);
        printf("Nome digitado: %s\n\n", nome);

        a = strlen(nome);

        for(i = 0; i < a; i++){
            printf("%c\n", nome[i]);
        }
        return 0;
}

