/*Quest�o 6: Ler um n�mero e diga se eles est� contido no intervalo entre 10 e 15, onde 10 e 15 tamb�m
pertencem ao intervalo.*/

#include<stdio.h>

int main(){
        /* Objetivo: Dizer se um numero est� contido no intervalo entre 10 e 15;
        1- Declarar uma vari�vel do tipo int;
        2- Leitura da variavel digitada pelo scanf;
        3- Dizer se pertence ao intervalo supracitado*/

        int numero = 0;

        scanf("%d", &numero);

        if(numero>=10 && numero<= 15){
            printf("O numero esta contido no intervalo entre 10 e 15");
        }else printf("O numero nao esta contido no intervalo");
        return 0;
}


