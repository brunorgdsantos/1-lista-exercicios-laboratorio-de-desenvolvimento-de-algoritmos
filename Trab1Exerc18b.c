/*Quest�o 18:
Ler 10 n�meros a serem digitados pelo usu�rio e armazen�-los em um vetor.
b)Pergunte ao usu�rio qual n�mero ele quer pesquisar (no lugar do n�mero 3), e diga quantas
vezes este n�mero est� presente no vetor.*/

#include<stdio.h>

int main(){
        /* Objetivo: Ler 10 numeros, armazena-los em um vetor e dizer qual numero o usu�rio quer pesquisar;
        1- Declarar quatro vari�veis do tipo int;
        2- Usar a estrutura de repeti��o do for;
        3- Mostrar quantos numeros 3 foram digitados*/

        int i = 0, m = 0, k = 0, x = 0;
        int num[10];

        printf("Digite dez numeros:\n");

        for(i = 0; i < 10; i++){
            scanf(" %d", &num[i]);
        }

        printf("Qual numero voce quer pesquisar?\n");
        scanf("%d", &m);

        for(k = 0; k < 10; k++){
            if(num[k] == m){
             x++;
            }
        }
        printf("Numero de vezes que aparece o %d: %d", m, x);
        return 0;
}







