/*Quest�o 5: Ler a altura e a base de um tri�ngulo e calcular sua �rea. A f�rmula de �rea de um tri�ngulo
� A = (base*altura)/2.*/

#include<stdio.h>

int main(){
        /* Objetivo: Calcular a �rea de um tri�ngulo;
        1- Declarar tr�s vari�veis do tipo float (var_altura, var_base, area);
        2- Leitura das variaveis digitada pelo scanf;
        3- Exibi��o da area do triangulo pelo printf*/

        float var_altura, var_base, area;

        scanf("%f", &var_altura);
        scanf("%f", &var_base);

        area = (var_base*var_altura)/2;

        printf("A = %.2f", area);
        return 0;
}

