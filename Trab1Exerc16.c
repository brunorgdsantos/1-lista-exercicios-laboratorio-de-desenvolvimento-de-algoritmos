/*Quest�o 16:
Leia um n�mero inteiro e diga se ele � um n�mero primo ou n�o.*/

#include<stdio.h>

int main(){
        /* Objetivo: Ler um numero inteiro e dizer se � primo ou n�o;
        1- Declarar uma vari�vel do tipo int;
        2- Usar a estrutura de repeti��o do while;
        3- Mostrar o resultado conforme solicitado pela quest�o*/

        int var1 = 0;
        char carac;

        do{
            printf("Digite um numero inteiro\n");
            scanf("%d", &var1);

            if(var1%1==0 && var1%var1==0 && var1%2!=0){
                printf("E um numero primo\n");
            }else printf("Nao e um numero primo\n");

            printf("Deseja continuar (s/n)\n");
            scanf(" %s", &carac);

        }while(carac == 's' || carac == 'S');

        return 0;
}




