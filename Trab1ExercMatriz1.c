/*Quest�o 1 MATRIx:
Gere uma matriz 3x4 de inteiros, e pe�a para o usu�rio do programa preench�-la
Em seguida exiba-a separando os elementos da mesma linha por �-� (tra�o) e separando as
linhas por nova linha.*/

#include<stdio.h>

int main(){
        /* Objetivo: Gerar uma matriz de inteiros preenchida pelo usu�rio;
        1- Declarar quatro vari�veis do tipo int;
        2- Usar a estrutura de repeti��o do for;
        3- Mostrar a matriz*/

        int i = 0, k = 0, w = 0;
        int num[12];

        for(k = 0; k < 3; k++){

            for(i = 0; i < 4; i++){
                scanf(" %d", &num[w]);
                w++;
            }
        }
        w = 0;
        for(k = 0; k < 3; k++){

            for(i = 0; i < 4; i++){
                printf("%d -", num[w]);
                w++;
            }
            printf("\n\n");
        }
        return 0;
}
