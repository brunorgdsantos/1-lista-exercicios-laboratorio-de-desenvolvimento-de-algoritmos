/*Quest�o 6: Para ler um n�mero inteiro e dizer se ele � impar ou par.*/

#include<stdio.h>

int main(){
        /* Objetivo: Dizer se um n�mero inteiro � impar ou par;
        1- Declarar uma vari�vel do tipo int;
        2- Leitura da variavel digitada pelo scanf;
        3- Dizer se � impar ou par*/

        int numero = 0;

        scanf("%d", &numero);

        if(numero%2==0){
            printf("O numero digitado eh par");
        }else printf("O numero digitado eh impar");
        return 0;
}



