/*Quest�o 4: Ler uma temperatura em graus Fahrenheit e apresent�-la convertida em graus Celsius. A
f�rmula de convers�o �: C = (F-32)*(5.0/9)*/

#include<stdio.h>

int main(){
        /* Objetivo: Apresentar uma temperatura de Fahrenheit para Celsius;
        1- Declarar duas vari�veis do tipo float (temperaturaf,var_celsius);
        2- Leitura da variavel digitada pelo scanf;
        3- Exibi��o da temperatura em celsius pelo printf*/

        float temperaturaf, var_celsius;

        scanf("%f", &temperaturaf);
        var_celsius = (temperaturaf-32)*(5.0/9);
        printf("%.2f\n", var_celsius);
        return 0;
}


