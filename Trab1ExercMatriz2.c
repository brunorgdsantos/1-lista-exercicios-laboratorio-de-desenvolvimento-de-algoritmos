/*Quest�o 2 MATRIx:
Gere uma matriz 3x4 de inteiros, e pe�a para o usu�rio do programa preench�-la.
Informe a quantidade de vezes que o n�mero 5 foi digitado. */

#include<stdio.h>

int main(){
        /* Objetivo: Gerar uma matriz de inteiros preenchida pelo usu�rio e informar quantas vezes o numero 5 aparece;
        1- Declarar cinco vari�veis do tipo int;
        2- Usar a estrutura de repeti��o do for e condicional if;
        3- Mostrar a matriz*/

        int i = 0, k = 0, w = 0, a = 0;
        int num[12];

        for(k = 0; k < 3; k++){

            for(i = 0; i < 4; i++){
                scanf(" %d", &num[w]);
                w++;
            }
        }
        w = 0;
        for(k = 0; k < 3; k++){

            for(i = 0; i < 4; i++){
                printf("%d -", num[w]);
                w++;

                if(num[w] == 5){
                    a++;
                }
            }
            printf("\n\n");
        }
        printf("O numero 5 aparece: %d", a);
        return 0;
}

