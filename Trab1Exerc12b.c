/*Quest�o 12:
A professora mandou que jo�ozinho escrevesse 500 vezes no quadro a frase: �Eu n�o vou
mais jogar avi�ezinhos de papel na sala de aula�.
a) Fa�a o que a professora pediu utilizando a estrutura for.
b) Fa�a o que a professora pediu utilizando a estrutura while*/

#include<stdio.h>

int main(){
        /* Objetivo: Escrever 500 vezes uma frase;
        1- Declarar uma vari�vel do tipo int;
        2- Usar a estrutura de repeti��o while;
        3- Mostrar o resultado*/

        int i = 0;

        while(i<500){
            printf("Eu nao vou mais jogar avioezinhos de papel na sala de aula\n");
            i++;
        }
        return 0;
}
