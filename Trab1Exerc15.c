/*Quest�o 15:
Desenhe na tela uma forma geom�trica utilizando caracteres, utilize um espa�o entre os
caracteres. Sugest�o de caracteres: X, 0, O.
a) Desenhe um quadrado de tamanho 5x5. Exemplo:*/

#include<stdio.h>

int main(){
        /* Objetivo: Desenhar na tela uma forma geometrica com uso de caracteres;
        1- Declarar uma vari�vel do tipo char;
        2- Declarar duas vari�veis de controle do tipo int (i, x);
        3- Usar a estrutura de repeti��o for;
        4- Mostrar o resultado conforme solicitado pela quest�o*/

        char carac;
        int i = 0, x = 0;

        printf("Escolha um caractere: Sugestao X, 0, O\n");
        scanf("%c", &carac);
        printf("\n\n");

        for(i = 0; i < 3; i++){
            for(x = 0; x < 3; x++){
                printf("%c ", carac);
            }
            printf("\n");
        }
        return 0;
}



