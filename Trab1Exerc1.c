/*Quest�o 1: Ler um n�mero e exibir a frase: �o n�mero digitado foi: � e logo ap�s esta frase apresentar
o valor do n�mero lido.*/

#include<stdio.h>

int main(){
        /* Objetivo: Exibir a frase: �o n�mero digitado foi: � e apresentar o valor do n�mero
        1- Declarar uma vari�vel numero para armazenar o numero digitado
        2- Leitura no numero digitado pelo scanf;
        3- Exibi��o do numero digitado pelo printf*/

        int numero = 0;

        scanf("%d", &numero);
        printf("o numero digitado foi: %d", numero);
        return 0;
}
